# Particle Filter

An implementation of a particle filter, aka Sequential Monte Carlo.

Done in Armadillo C++ to learn the bindings.  Note that compile may be very slow due to armadillo templating; this is to be expected.

