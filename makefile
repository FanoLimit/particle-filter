CC=g++
CFLAGS=-std=c++11 -larmadillo -lopenblas -llapack

TARGET=particle

all: $(TARGET)

$(TARGET): $(TARGET).cpp
	$(CC) -o $(TARGET) $(TARGET).cpp $(CFLAGS)

clean:
	$(RM) $(TARGET)
