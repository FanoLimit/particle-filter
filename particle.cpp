#include <armadillo>
#include <RcppArmadillo.h>
#include <stdio.h>
#include <cmath>
#include<stdlib.h>

/* An implementation of a Particle Filter to get used to 
 * arma functionality and general fun.  
 * Implementation based on the following references:
 *
 * Douc, R. & Cappe, O. Comparison of resampling schemes for particle filtering. 
 * https://arxiv.org/pdf/cs/0507025.pdf
 * 
 * Liu, J. & Chen, R.  Sequential Monte Carlo Methods for Dynamic Systems.
 * https://www.jstor.org/stable/2669847
 */

/*  RNG for a Multivariete normal distribution
 *  Implements using the slower but more stable
 *  Eigendecomposition instead of the Cholesky
 *  decomposition.
 *
 * Params: 
 *  n: number of samples to be drawn
 *  mu: vector of means for each dimension
 *  sigma: matrix of pairwise 1/varience
 */

arma::mat mvnormsample(const int& n, const arma::vec& mu, const arma::mat& sigma){
    arma::vec eigVal;
    arma::mat eigVec; //Stores the eigenval and eigenvec

    arma::eig_sym(eigVal,eigVec,sigma);

    double minEigVal=eigVal.min();

    if (minEigVal<0){
        printf("Matrix is not positive semidefinite.\n");
        exit(EXIT_FAILURE);
    }

    int ncols=sigma.n_cols;

    arma::mat gauss=arma::randn(n,ncols);
    arma::mat eigVec_t=eigVec.t();
    
    arma::mat decomp=eigVec*(eigVec_t.each_col() % sqrt(eigVal));
    return arma::repmat(mu,1,n).t()+gauss*decomp.t();
}

/*Implements multinomoial stratified resampling.
 * Params:
 *
 * const n: number of samples to be drawn
 * arma::colvec& weights: Weighting vector
 * arma::mat& x: Matrix to be sampled from and written to
 *
 * Note weights must be normalized
 *
 */


void multinomialrs(const int n, arma::colvec& weights, arma::mat& x){
    arma::colvec r(n); r.randu();
    arma::colvec cdf=arma::cumsum(weights);
    arma::uvec index(n);

    for (int i=0;i<n;i++){
        arma::uvec idx=find(cdf<r(i)); //Creates a vec to hold all search results
        index(i)=idx.n_elem+1;         //Arma has no binsearch functionality for 
    }                                  //some reason
        
    x=x.rows(index-1);
}

/* Implements multinomial stratified resampling
 * using the inverse CDF method.  Better asymtotic
 * time complexity but might not be as easily
 * paralelizable
 *
 * Params:
 * const n: number of samples to be drawn
 * arma::colvec weights: Weighting vector
 * arma::mat x: Sample Matrix
 */

arma::mat multinomialrs2(const int n, arma::colvec& weights, arma::mat& x){
    arma::colvec r(n);r.randu();
    /* Loop which breaks easy paralelism, but you can probably optimize
    *  using a perturb matrix approach.  Potential numerical instability
    *  from pow and inverse.  Probably better to also fix the root-pow-
    *  root cycle using a place holder vec and moving it over
    */

    r(n)=(pow(n),((double)1/(double)n))
    for (int i=n-2;i>0;i--){
        r(i)=pow(r(i+1),i+2)*pow(r(i),(double) 1 / (double) i);
    }


    arma::mat xx;
    arma::colvec cdf=cumsum(weights);

    for (int i=0;i<n;i++){
        arma::colvec index=find(weights,u(i));
        xx.rows(i)=x.rows(index.n_elem + 1);
    }
    return xx;
}

/* Implements Systemic Resampling.
 *
 * Params:
 * n: number of samples to be drawn
 * weights: prenormalized weight vector
 * x: sample vector
 */

void systemicsample(const int n, arma::colvec& weights, arma::mat& x){
    arma::colvec r(n);r.randu();
    r=(regspace<vec>(0,n-1) + arma::as_scalar(r))/n;
    arma::colvec cdf=arma::cumsum(weights);
    arma::uvec idx(n);

    for (int i=0;i<n;i++){
        arma::vec indexes=arma::find(weights,r(i));
        
/* Implements Stratified Resampling.  Faster if paralelism is available, but
 * requires some performance tuning and some additional calculations to
 * rebalance the weights every cycle.  Basic implementation here; a more
 * advanced implementation would take into account numerical instability
 * when balancing. 
 *
 * Params:
 *
 * n: number of samples to be drawn
 * weights: Prenormalized weight vector
 * x: Sample matrix
 *
 */

void stratifiedresample(const int n, arma::colvec& weights, arma::mat& x){
    arma::colvec r(n); r.randu();
    r=(<regspace<vec>(0,n-1) + r ) /n;

    arma::colvec cdf=arma::cumsum(weights);

    arma::uvec idxs(n);

    for (int i=0;i<n;i++){
        arma::uvex indexes=find(cdf<r(i));
        idxs(i)=indexes.n_elem + 1;
    }
    x=x.rows(indxs-1);
}

/* Implements the Particle Filter using stratified resampling.
 *
 * Params:
 *
 * arma::mat& y: Data of dim n x d of n observations, each of dimensionality d
 * arma::mat& init0: Matrix of initial guesses
 * arma::mat& cov0: Matrix of cov of init0
 * arma::mat& eps: Matrix of measurement noise in init0
 * arma::mat& proseps: Matrix of process noise
 * arma::mat& inputcontrol: Optional matrix specifying control values.  Should match y dimensions
 * const int& n: number of particles
 * double& nThreshold: Floor before resampling begins
 */

List PFcpp(const arma::mat& y, Rcpp::List mod, const int& N,
           Rcpp::Nullable<Rcpp::NumericMatrix> inputControl = R_NilValue,
           int whichGGfunction = 1, int whichFFfunction = 1,
           double Nthreshold = 1e+9, std::string resampling = "strat",
           bool roughening = false, double Grough = .2, 
           bool MCparticles = true, bool logLik = false,
           bool pseudoInverse = false, bool simplify = true){
  
  arma::colvec m0 = as<arma::colvec>(mod["m0"]);
  arma::mat C0 = as<arma::mat>(mod["C0"]);
  arma::mat V = as<arma::mat>(mod["V"]);
  arma::mat W = as<arma::mat>(mod["W"]);
  
  int ym = y.n_cols;
  int yobs = y.n_rows;
  int p = m0.n_rows;
  
  arma::mat m(yobs+1, p, fill::zeros);
  m.row(0) = m0.t();
  arma::mat a(yobs, p, fill::zeros);
  arma::mat f(yobs, ym, fill::zeros);

  arma::cube C(C0.n_rows, C0.n_cols, yobs+1, fill::zeros);
  arma::cube R(p, p, yobs, fill::zeros);

  //handle the control input
  arma::mat inputControlMat;
  
  if (inputControl.isNotNull()) {
    Rcpp::NumericMatrix icmat(inputControl);
    inputControlMat = Rcpp::as<arma::mat>(icmat);
    } else {
      inputControlMat.zeros(1, p);
      }
    
  int inputControlMatRows = inputControlMat.n_rows;  
  
  //initialize log-likelihood
  double ll = 0;
  
  //compute inverse of V:
  //- matrix V has to be non-singular
  //- condsider using the pseudo-inverse in case of a nearly singular V matrix

  arma::mat invV(V);
  if (pseudoInverse) {
    invV = arma::pinv(V);
    } else {
      invV = arma::inv_sympd(V);
      }
    
  
  //generate particles at t=0
  arma::mat xp = rmvnormCPP(N, m.row(0).t(), C0);
  //importance weights at t=0
  arma::colvec w(N);
  w.fill( 1 / (double) N );
  
  //importance weights before resampling at each time step
  arma::mat wt;
  //particles at each time step
  arma::cube xpt;
  
  if (MCparticles) {
    //intialize matrices
    wt.zeros(yobs + 1, N);
    xpt.zeros(N, p, yobs + 1);
    //particles at t=0
    wt.row(0) =  w.t();
    //importance weights at t=0
    xpt.slice(0) = xp;
    }

  C.slice(0) = C0;
  
  for (int i = 1; i < yobs + 1; i++) {
    
    //check for NA's
    arma::rowvec ayi = y.row(i-1);
    NumericVector yi = wrap(ayi);
    LogicalVector whereNA = is_na(yi);
    
    //control input
    arma::rowvec iCi(p);
    
    if (inputControlMatRows == 1) {
      iCi = inputControlMat;
      } else {
        iCi = inputControlMat.row(i-1);
        }
    
    if (is_false(any(whereNA))) {
      
      
      // //time update
      arma::mat wks = rmvnormCPP(N, arma::colvec(p, fill::zeros), W);
      //a priori state estimate
      for (int j = 0; j < N; j++) {
        xp.row(j) = GGfunction(xp.row(j), i, whichGGfunction) + iCi + wks.row(j);
        }
      a.row(i-1) = w.t() * xp;
      //covariance of a priori state estimate
      arma::mat xc = xp.each_row() - a.row(i-1);
      arma::mat swxc = xc.each_col() % sqrt(w);
      R.slice(i-1) = swxc.t() * swxc;
      
      
      // //measurement update
      arma::mat vks = rmvnormCPP(N, arma::colvec(ym, fill::zeros), V);
      //predicted measurement
      arma::mat yp(N, ym);
      for (int j = 0; j < N; j++) {
        yp.row(j) = FFfunction(xp.row(j), i, ym, whichFFfunction) + vks.row(j);
        }
      f.row(i-1) = w.t() * yp;
      //covariance of predicted measurement
      arma::mat yc = yp.each_row() - f.row(i-1);
      arma::mat swyc = yc.each_col() % sqrt(w);
      arma::mat Qy = swyc.t() * swyc;
      
      
      // //a posteriori estimates
      //importance weights update
      for (int j = 0; j < N; j++) {
        arma::rowvec xs = y.row(i-1) - yp.row(j);
        double invVxs = as_scalar(trunc_exp (-.5 * (xs * invV * xs.t() )));
        w(j) = w(j) * invVxs;
        }
      //note: weights are set to NaN when sum(w)=0
      if (sum(w) == 0) {
        Rcpp::warning("Importance weights for all particles are zero.\n  As a result, NaN in filter predictions / estimates.\n");
      }
      //normalize
      w = w / sum(w);
      //a posteriori state estimate
      m.row(i) = w.t() * xp;
      //a posteriori error covariance
      arma::mat swxc2 = xc.each_col() % sqrt(w);
      C.slice(i) = swxc2.t() * swxc2;
      
      if (MCparticles) {
        wt.row(i) = w.t();
        xpt.slice(i) = xp;
        }
      
      
      // //resampling
      double Neff = 1 / as_scalar(w.t() * w);
      
      if (Neff < Nthreshold) {
        if (resampling=="mult") {
          resampleMcpp(N, w, xp);
          w.fill( 1 / (double) N );
        } else if (resampling=="sys") {
          resampleSycpp(N, w, xp);
          w.fill( 1 / (double) N );
        } else {
          resampleSrcpp(N, w, xp);
          w.fill( 1 / (double) N );
        }
        }
       
       
       // //roughening
       if (roughening) {
         arma::mat El = range(xp, 0);
         arma::mat sigma_l = Grough * El * pow( (double) N, (-1/ (double) p) );
         arma::mat Jk = diagmat( pow (sigma_l, 2) );
         arma::mat ck = rmvnormCPP(N, arma::colvec(p, fill::zeros), Jk);
         xp = xp + ck;
         }
       
       
       // //compute log-likelihood
       if (logLik) {
         arma::mat e = y.row(i-1) - f.row(i-1);
         arma::mat logEigenQy = log(eig_sym(Qy));
         ll = ll + (double) ym * log( 2 * arma::datum::pi) +
           as_scalar(sum(logEigenQy)) + as_scalar(e * inv(Qy) * e.t());
         }
       
       
      } else {
        if (is_true(all(whereNA))) {
          
          // //time update
          arma::mat wks = rmvnormCPP(N, arma::colvec(p, fill::zeros), W);
          //a priori state estimate
          for (int j = 0; j < N; j++) {
            xp.row(j) = GGfunction(xp.row(j), i, whichGGfunction) + iCi + wks.row(j);
          }
          a.row(i-1) = w.t() * xp;
          //covariance of a priori state estimate
          arma::mat xc = xp.each_row() - a.row(i-1);
          arma::mat swxc = xc.each_col() % sqrt(w);
          R.slice(i-1) = swxc.t() * swxc;
          
          
          // //measurement update
          arma::mat vks = rmvnormCPP(N, arma::colvec(ym, fill::zeros), V);
          //predicted measurement
          arma::mat yp(N, ym);
          for (int j = 0; j < N; j++) {
            yp.row(j) = FFfunction(xp.row(j), i, ym, whichFFfunction) + vks.row(j);
          }
          f.row(i-1) = w.t() * yp;
          
          // //a posteriori estimates
          //a posteriori state estimate
          m.row(i) = a.row(i-1);
          //a posteriori error covariance
          C.slice(i) = R.slice(i-1);
          
          //since there are no measurements available, the importance
          //weights remain unchanged
          
          if (MCparticles) {
            wt.row(i) = w.t();
            xpt.slice(i) = xp;
          }
    
          } else {
            arma::uvec good = find_finite(y.row(i-1));
            
            // //time update
            arma::mat wks = rmvnormCPP(N, arma::colvec(p, fill::zeros), W);
            //a priori state estimate
            for (int j = 0; j < N; j++) {
              xp.row(j) = GGfunction(xp.row(j), i, whichGGfunction) + iCi + wks.row(j);
            }
            a.row(i-1) = w.t() * xp;
            //covariance of a priori state estimate
            arma::mat xc = xp.each_row() - a.row(i-1);
            arma::mat swxc = xc.each_col() % sqrt(w);
            R.slice(i-1) = swxc.t() * swxc;
            
            
            // //measurement update
            arma::mat vks = rmvnormCPP(N, arma::colvec(ym, fill::zeros), V);
            //predicted measurement
            arma::mat yp(N, ym);
            for (int j = 0; j < N; j++) {
              yp.row(j) = FFfunction(xp.row(j), i, ym, whichFFfunction) + vks.row(j);
            }
            f.row(i-1) = w.t() * yp;
            //covariance of predicted measurement
            arma::mat ypGood = yp.cols(good);
            arma::rowvec fAll = f.row(i-1);
            arma::rowvec fGood = fAll.elem(good);
            arma::mat yc = ypGood.each_row() - fGood;
            arma::mat swyc = yc.each_col() % sqrt(w);
            arma::mat Qy = swyc.t() * swyc;
            
            
            // //a posteriori estimates
            //importance weights update
            arma::mat Vgood = V.submat(good, good);
            arma::mat invVgood = arma::inv_sympd(Vgood);
            arma::mat yGood = y.cols(good);
            
            for (int j = 0; j < N; j++) {
              arma::rowvec xsGood = yGood.row(i-1) - ypGood.row(j);
              double invVxs = as_scalar(trunc_exp ( -.5 * (xsGood * invVgood * xsGood.t() )));
              w(j) = w(j) * invVxs;
            }
            //note: weights are set to NaN when sum(w)=0
            if (sum(w) == 0) {
              Rcpp::warning("Importance weights for all particles are zero.\n  As a result, NaN in filter predictions / estimates.\n");
            }
            //normalize
            //note: weights are set to NaN when sum(w)=0 
            w = w / sum(w);
            //a posteriori state estimate
            m.row(i) = w.t() * xp;
            //a posteriori error covariance
            arma::mat swxc2 = xc.each_col() % sqrt(w);
            C.slice(i) = swxc2.t() * swxc2;
            
            if (MCparticles) {
              wt.row(i) = w.t();
              xpt.slice(i) = xp;
            }
            
            
            // //resampling
            double Neff = 1 / as_scalar(w.t() * w);
            
            if (Neff < Nthreshold) {
              if (resampling=="mult") {
                resampleMcpp(N, w, xp);
                w.fill( 1 / (double) N );
              } else if (resampling=="sys") {
                resampleSycpp(N, w, xp);
                w.fill( 1 / (double) N );
              } else {
                resampleSrcpp(N, w, xp);
                w.fill( 1 / (double) N );
              }
            }
            
            
            // //roughening
            if (roughening) {
              arma::mat El = range(xp, 0);
              arma::mat sigma_l = Grough * El * pow( (double) N, (-1/ (double) p) );
              arma::mat Jk = diagmat( pow (sigma_l, 2) );
              arma::mat ck = rmvnormCPP(N, arma::colvec(p, fill::zeros), Jk);
              xp = xp + ck;
            }
            
            
            // //compute log-likelihood
            if (logLik) {
              arma::mat e = yGood.row(i-1) - fGood;
              arma::mat logEigenQy = log(eig_sym(Qy));
              ll = ll + good.n_elem * log( 2 * arma::datum::pi) +
                as_scalar(sum(logEigenQy)) + as_scalar(e * inv(Qy) * e.t());
            }
            }
          }
      }
    

  Rcpp::List ans = Rcpp::List::create(Rcpp::Named("mod") = mod,
                                      Rcpp::Named("N") = N,
                                      Rcpp::Named("m") = m,
                                      Rcpp::Named("C") = C,
                                      Rcpp::Named("a") = a,
                                      Rcpp::Named("R") = R,
                                      Rcpp::Named("f") = f,
                                      Rcpp::Named("whichGGfunction") = whichGGfunction,
                                      Rcpp::Named("whichFFfunction") = whichFFfunction,
                                      Rcpp::Named("inputControl") = inputControlMat);
  
  if (logLik) {
    ans["logLik"] = .5*ll;
  }
  
  if (MCparticles) {
    ans["xpt"] = xpt;
    ans["wt"] = wt;
  }
  
  if (simplify) {
    return(ans);
  } else {
    ans["y"] = y;
    return(ans);
  }
 
}



//function for drawing a sample of size 1 based on given probabilities
int sample1(arma::vec probs) {
  arma::vec cumprob = cumsum(probs);
  arma::vec randnum = randu(1);
  
  arma::uvec whichNumbers = find(cumprob <= as_scalar(randnum));
  int whichElement = whichNumbers.n_elem;
  
  return(whichElement);
}



//Implementation of the backward particle smoother
// [[Rcpp::export]]
List PFsmoothcpp (Rcpp::List filterData, int Ntrajectories = 100){
  
  Rcpp::List mod = filterData["mod"];
  arma::mat W = as<arma::mat>(mod["W"]);
  arma::mat mod_m = as<arma::mat>(filterData["m"]);
  arma::mat mod_a = as<arma::mat>(filterData["a"]);
  arma::mat mod_wt = as<arma::mat>(filterData["wt"]);
  arma::cube mod_xpt = as<arma::cube>(filterData["xpt"]);
  arma::mat mod_inputControl = as<arma::mat>(filterData["inputControl"]);
  int N = filterData["N"];
  int whichGGfunction = filterData["whichGGfunction"];
  
  int n = mod_m.n_rows-1;
  int p = mod_m.n_cols;
  arma::cube s(n+1, p, Ntrajectories, fill::zeros);
    
  //compute inverse of W:
  //- matrix W has to be non-singular
  //- use the pseudo-inverse for more stability
  arma::mat invW = arma::pinv(W);
  
  //backward particle smoother
  for (int i = 0; i < Ntrajectories; i++) {
    arma::mat si(n+1, p);
    
    arma::mat xpti(N, p);
    arma::vec wti(N);
    
    xpti = mod_xpt.slice(n);
    wti = mod_wt.row(n).t();
    
    int whichParticle1 = sample1(wti);
    si.row(n) = xpti.row(whichParticle1);
    
    for (int j = n-1; j >=0 ; j--) {
      
      xpti = mod_xpt.slice(j);
      wti = mod_wt.row(j).t();
      
      //handle control input
      arma::rowvec iCi(p);
      
      if (mod_inputControl.n_rows == 1) {
        iCi = mod_inputControl;
      } else {
        iCi = mod_inputControl.row(j);
      }
      
      //compute backward smoothing
      arma::colvec wi(N);
      for (int k = 0; k < N; k++) {
        arma:rowvec fx = GGfunction(xpti.row(k), j+1, whichGGfunction) + iCi;
        arma::rowvec xs = si.row(j+1) - fx;
        //note for an invW matrix with large numbers, or when xs
        //has large entries, then invWxs might become 0 
        double invWxs = as_scalar(trunc_exp (-.5 * (xs * invW * xs.t() )));
        wi(k) = wti(k) * invWxs;
      }
      
      //note: weights are set to NaN when sum(wi)=0
      if (sum(wi) == 0) {
        Rcpp::stop("Importance weights for all particles are zero\n");
      }
      
      //normalize
      wi = wi / sum(wi);
      
      int whichParticle2 = sample1(wi);
      si.row(j) = xpti.row(whichParticle2);
    }
    
    s.slice(i) = si;
  }
  
  Rcpp::List ans = Rcpp::List::create(Rcpp::Named("s") = s);
  return(ans);
}
