int main(){
    arma::running_stat_vec<arma::rowvec> stats(true);

    for (int i=0;i<10000;i++){
        arma::vec mu(20);
        mu.ones();

        arma::mat sigma(20,20);
        sigma.diag().ones();

        arma::mat sample=mvnormsample(1,mu,sigma);
        stats(sample);
    }

    std::cout<<"Covariance Matirx"<<std::endl;
    std::cout<<stats.cov()<<std::endl;

    std::cout<<"Mean"<<std::endl<<stats.mean()<<std::endl;

    return 0;
}

